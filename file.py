#!/usr/bin/env python3

# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.element import Element, Section
from preprocessor.element.namespace import Namespace
from preprocessor.element.structlike.component import Component
from preprocessor.element.structlike.archetype import Archetype
from preprocessor.element.structlike.system import System
from preprocessor.element.structlike.application import Application


class File(Element):
    """
    The base element for the preprocessor, only created by the front
    end and acts as a body to contain other elements. As the name
    suggests it encapsulates a source code file after the C++
    preprocessor has been executed.
    """
    def __init__(self, content: str):
        super().__init__(None, {"body": Section(content, [Namespace, Component, Archetype, System, Application])}, "")

    def compile(self) -> str:
        return self.section("body").compile()
