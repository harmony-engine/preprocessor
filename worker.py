# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from typing import List
from threading import Thread


threads = 1


def set_threads(n_threads: int):
    global threads
    threads = n_threads


def parallel(work):
    def func(work_list: List):
        work_list_copy = work_list[:]
        workers = []

        def worker():
            while True:
                try:
                    task = work_list_copy.pop(0)
                except IndexError:
                    return
                work(task)

        for i in range(threads):
            thread = Thread(target=worker)
            workers.append(thread)
            thread.start()

        for worker in workers:
            worker.join()

    return func
