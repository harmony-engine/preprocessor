find_package(Python3 REQUIRED)

set(CREATE_SCRIPT ${CMAKE_BINARY_DIR}/preprocessor_venv_create)
file(WRITE ${CREATE_SCRIPT}
"   ${Python3_EXECUTABLE} -m venv ${CMAKE_BINARY_DIR}/preprocessor-venv
    source ${CMAKE_BINARY_DIR}/preprocessor-venv/bin/activate
    pip install -r ${CMAKE_SOURCE_DIR}/preprocessor/requirements.txt
    deactivate
")
file(CHMOD ${CREATE_SCRIPT} PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)
add_custom_command(
    OUTPUT preprocessor_venv
    DEPENDS ${CREATE_SCRIPT}
    COMMAND source ${CREATE_SCRIPT}
    COMMENT Creating Python venv for preprocessor
)

function(preprocess includes raw_source processed_source)
    set(FILE_COUNTER 0)
    foreach(SOURCE IN LISTS raw_source)
        pad_string(PFC ${FILE_COUNTER} 0 8)
        cmake_path(GET SOURCE FILENAME SOURCE_NAME)
        string(PREPEND SOURCE_NAME ${PROJECT_BINARY_DIR}/${PFC}-)
        list(APPEND OUT_SOURCE_FILES ${SOURCE_NAME})
        MATH(EXPR FILE_COUNTER "${FILE_COUNTER}+1")
    endforeach()

    set(PROCESS_SCRIPT ${PROJECT_BINARY_DIR}/preprocess)
    string(REPLACE ";" " " RAW_SOURCE "${raw_source}")
    string(REPLACE ";" " " INCLUDES "${includes}")
    ProcessorCount(CORES)
    file(WRITE ${PROCESS_SCRIPT}
    " 	source ${CMAKE_BINARY_DIR}/preprocessor-venv/bin/activate
        ${CMAKE_SOURCE_DIR}/preprocessor/main.py \
            --out_dir ${PROJECT_BINARY_DIR} \
            --files ${RAW_SOURCE} \
            --includes ${PROJECT_SOURCE_DIR} ${INCLUDES} \
            --jobs ${CORES}
        deactivate
    ")
    file(CHMOD ${PROCESS_SCRIPT} PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)

    add_custom_command(
        OUTPUT ${OUT_SOURCE_FILES}
        DEPENDS preprocessor_venv ${CREATE_SCRIPT}
	    COMMAND source ${PROCESS_SCRIPT}
    )

    set(${processed_source} "${OUT_SOURCE_FILES}" PARENT_SCOPE)
endfunction()
