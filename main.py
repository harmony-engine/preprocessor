#!/usr/bin/env python3

# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


if __name__ != "__main__":
    raise Exception("It does not make sense to import this file. Run it through the command line!")

# Ugly hack to allow the main file to be placed inside the module
from sys import path
from os.path import dirname

path.append(dirname(path[0]))

from cProfile import run
from argparse import ArgumentParser
from preprocessor import process


def main():
    parser = ArgumentParser(
        description="Transform special Harmony Engine constructs into standard C++. Should not be called directly, "
                    "but through the build system (eg.CMake)."
    )
    parser.add_argument("--out_dir", required=True, type=str, help="Where processed files should go")
    parser.add_argument("--includes", default=[], nargs="+", type=str, help="Directories to find header files")
    parser.add_argument("--files", required=True, nargs="+", type=str, help="Source files to preprocess")
    parser.add_argument("--jobs", default=1, type=int, help="Amount of threads to spawn")
    args = parser.parse_args()

    process(args.out_dir, args.includes, args.files, args.jobs)


main()
