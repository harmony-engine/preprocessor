# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.element import Element
from preprocessor.element.annotation import Annotation
from regex import compile
from typing import List, Optional


class Tag(Annotation):
    """
    An annotation which looks like a function, generally placed
    before a function or class
    """
    name: str
    args: List[str]

    # language=regexp
    re = compile(r"(\w+)\s*(?:\(([\s\S]*?)\))?")

    def __init__(self, parent: Element, groups: List[str]):
        self.name = groups[0]
        if groups[1] is None:
            self.args = []
        else:
            self.args = groups[1].split(",")
        super().__init__(parent, {}, parent.namespace)


class TagPrefixed(Annotation):
    """
    A tag which is preceded by a @ to differentiate it from a
    function call
    """

    # language=regexp
    re = compile(r"@(\w+)\s*(?:\(([\s\S]*?)\))?")


def find_tag(tags: List[Tag], name: str) -> List[Tag]:
    return list(filter(lambda x: x.name == name, tags))


def find_tag_unique(tags: List[Tag], name: str) -> Optional[Tag]:
    found = find_tag(tags, name)
    if len(found) > 1:
        raise Exception("Multiple tags of the same name found")
    elif len(found) == 1:
        return found[0]
    else:
        return None
