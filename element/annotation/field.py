# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.element import Element
from preprocessor.element.annotation import Annotation
from regex import compile
from typing import List


class Field(Annotation):
    """Represents a field in a struct eg `int foo;`"""
    type: str
    name: str

    # language=regexp
    re = compile(r"\s*(.+?)\s+(\w+?);")

    def __init__(self, parent: Element, groups: List[str]):
        self.type = groups[0]
        self.name = groups[1]
        super().__init__(parent, {}, parent.namespace)
