# Copyright (C) 2020 Evan La Fontaine                                                                                                                                                                                                        
#                                                                                                                                                                                                                                            
# This program is free software: you can redistribute it and/or modify                                                                                                                                                                       
# it under the terms of the GNU General Public License as published by                                                                                                                                                                       
# the Free Software Foundation, either version 3 of the License, or                                                                                                                                                                          
# (at your option) any later version.                                                                                                                                                                                                        
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.expressions import nested
from preprocessor.element import Element, Section
from preprocessor.element.structlike.component import Component
from preprocessor.element.structlike.archetype import Archetype
from preprocessor.element.structlike.system import System
from preprocessor.element.structlike.application import Application

from regex import compile
from typing import List, Union


class Namespace(Element):
    """
    A C++ namespace
    """
    name: str

    # language=regexp
    re = compile(r"namespace\s+((?:\w|::)*?)\s*" + nested(2))

    def __init__(self, parent: Union[None, Element], groups: List[str]):
        self.name = groups[0]
        super().__init__(
            parent,
            {"body": Section(groups[1], [Namespace, Component, Archetype, System, Application])},
            parent.namespace + "::" + self.name
        )

    def compile(self):
        # language=C++
        return f"""
        namespace {self.name} {{
            {self.section("body").compile()}
        }}
        """
