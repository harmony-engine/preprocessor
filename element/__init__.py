# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from __future__ import annotations

from re import Match
from uuid import uuid4
from typing import List, Dict, Union
from abc import abstractmethod, ABCMeta


class Section:
    __body: str
    __types: List
    __children: Dict[str, Element]

    def __init__(self, body: str, types: List):
        self.__body = body
        self.__types = types
        self.__children = {}

    def children(self):
        return self.__children.values()

    def procreate(self, parent: Element):
        for child_type in self.__types:
            self.__body = child_type.re.sub(child_type.repl(parent, self.__children), self.__body)

    def compile(self) -> str:
        """
        Translate element into legal C++ recursively
        """
        res = self.__body
        for child in self.__children:
            res = res.replace(child, self.__children[child].compile())
        return res


class Element:
    """
    The base class for all code elements in Harmony C++ which require
    special treatment, either because information must be extracted or
    it must be translated into legal C++.
    """

    __metaclass__ = ABCMeta

    __sections: Dict[str, Section]
    namespace: str
    _parent: Union[None, Element]

    def __init__(self, parent: [None, Element], sections: Dict[str, Section], namespace: str):
        self.__sections = sections
        self.namespace = namespace
        self._parent = parent
        for section in self.__sections.values():
            section.procreate(self)

    @classmethod
    def repl(cls, parent: Element, siblings: Dict[str, Element]):
        """
        Function which generates the function used to handle regex
        substitutions which occur when a child is matched, replacing
        the element with a UUID which can be substituted back at
        compile time
        """
        def _repl(match: Match) -> str:
            tag = str(uuid4())
            siblings[tag] = cls(parent, match.groups())
            return tag
        return _repl

    def section(self, name: str) -> Section:
        return self.__sections[name]

    @abstractmethod
    def compile(self) -> str:
        pass
