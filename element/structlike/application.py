# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.element import Section
from preprocessor.element.structlike import structlike, Structlike
from preprocessor.element.annotation.tag import Tag, find_tag_unique
from typing import Tuple
from functools import reduce
from typing import Dict
from uuid import uuid4


found = False


@structlike("application", True, False)
class Application(Structlike):
    """
    Defines the harmony application
    """
    name: str
    version: Tuple[int, int, int]
    systems: Dict[str, str]

    def __init__(self, name: str, tags: str, body: str, namespace: str):
        global found
        if found:
            raise Exception("Multiple definitions of Application are not permitted")
        found = True
        super().__init__({
            "tags": Section(tags, [Tag]),
            "body": Section(body, [])
        }, namespace + "::" + name)
        if name != "":
            raise Exception("Malformed application struct, name must be defined with a tag")
        parsed_tags = self.section("tags").children()
        name_tag = find_tag_unique(parsed_tags, "name")
        self.name = "\"\"" if name_tag is None else name_tag.args[0]
        version_tag = find_tag_unique(parsed_tags, "version")
        self.version = (0, 0, 0) if version_tag is None else \
            (int(version_tag.args[0]), int(version_tag.args[1]), int(version_tag.args[2]))

        self.systems = {}
        for sys in map(lambda x: x.strip(), body.split(",")):
            self.systems["_" + str(uuid4()).replace("-", "_")] = sys

    def compile(self) -> str:
        # language=C++
        def on_all(func: str):
            return reduce(lambda x, y: x + f"""
            {y}{func};
            """, self.systems.keys(), "")

        fields = reduce(lambda x, y: x + f"""
        {y[1]} {y[0]};
        """, self.systems.items(), "")

        app_name = "_" + str(uuid4()).replace("-", "_")
        app_type = "_" + str(uuid4()).replace("-", "_")
        system = "_" + str(uuid4()).replace("-", "_")

        # language=C++
        templates_inner = reduce(lambda x, y: x + f"""
        template<> {y[1]}& {system}<{y[1]}>() {{
            return {app_name}.{y[0]};
        }}
        """, self.systems.items(), "")

        # language=C++
        templates = reduce(lambda x, y: x + f"""
        template {y[1]}& Harmony::system<{y[1]}>();
        """, self.systems.items(), "")

        # language=C++
        return f"""
        class {app_type} : public Harmony::Internal::Application {{
        public:
            inline void initialise() override {{
                {on_all(".initialise()")}
            }};
            inline void update() override {{
                {on_all("()")}
            }};
            inline void decommission() override {{
                {on_all(".decommission()")}
            }};
            
            {fields}
        }};
        
        {app_type} {app_name} = {app_type}();
        Harmony::Internal::Application* Harmony::Internal::application = &{app_name};
        const Harmony::Internal::ApplicationData Harmony::application_data = {{
            .name = {self.name},
            .version = {{
                .major = {self.version[0]},
                .minor = {self.version[1]},
                .patch = {self.version[2]}
            }}
        }};
        
        template<typename T> T& {system}();
        
        {templates_inner}
        
        template<typename T> T& Harmony::system() {{
            return {system}<T>();
        }}
        
        {templates}
        """
