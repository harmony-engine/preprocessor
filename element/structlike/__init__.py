# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from abc import ABC
from preprocessor.element import Element, Section
from preprocessor.expressions import nested
from preprocessor.worker import parallel
from regex import compile
from typing import List, Union, Dict

structs = []


class Structlike(Element, ABC):
    """
    An element which acts as the base type for things which look like structs,
    that is both in the general structure, but also in how they are defined in
    headers so appear many times in a project. As the preprocessor uses a global
    state in some sections (eg. a field index) or randomness (eg. function names
    from UUIDs) all reoccurrences should be combined with the structure only
    being compiled once.
    """

    compiled: Union[None, str]

    def __init__(self, sections: Dict[str, Section], namespace: str):
        global structs
        super().__init__(None, sections, namespace)
        structs.append(self)

    @staticmethod
    def compile_all():
        """
        Convert all the elements into actually C++ code
        """
        global structs

        @parallel
        def compile_struct(struct):
            struct.compiled = struct.compile()

        compile_struct(structs)


find_dict = {}


class StructlikeInstance(Element):
    """
    Type which is injected to point to the unique structlike element
    """

    def __init__(self, parent: Element, groups: List[str], shared_type):
        global find_dict
        super().__init__(parent, {}, parent.namespace + "::" + groups[0])

        if self.namespace not in find_dict:
            find_dict[self.namespace] = shared_type(groups[0], groups[1], groups[2], self.namespace)
        self.shared = find_dict[self.namespace]

    @staticmethod
    def find(name: str, usings: List[str]) -> Structlike:
        """
        Try to resolve the name of a structlike element taking into account
        using statements
        """
        global find_dict
        for using in usings:
            full_name = using + "::" + name
            if full_name in find_dict:
                return find_dict[full_name]
        raise Exception("Identifier not found")

    def compile(self) -> str:
        """
        Resolve the reference to the unique structure element which has
        been compiled
        """
        if self.shared.compiled is None:
            raise Exception("Structlike has not been compiled")
        return self.shared.compiled


def structlike(name: str, tagged: bool = False, named: bool = True):
    """
    Injects the element which points to the unique element replacing the
    structure of that element
    """

    def decorator(cls):
        """
        Create the actual function decorator
        """

        class Elem(StructlikeInstance):
            """
            Class which overrides the element replacing it with a reference
            """
            # language=regexp
            re = compile(
                name
                + (r"\s+(\w+)" if named else "()")
                + (r"\s*((?::(?:[^{]*))|(?:))" if tagged else r"(\s*)")
                + nested(3) + ";"
            )

            def __init__(self, parent: Union[None, Element], groups: List[str]):
                super().__init__(parent, groups, cls)

        return Elem

    return decorator
