# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.element import Section
from preprocessor.element.annotation.tag import Tag
from preprocessor.element.structlike import structlike, Structlike
from preprocessor.element.structlike.archetype import archetypes
from typing import List
from functools import reduce


@structlike("system", True)
class System(Structlike):
    """
    A singleton object which runs an operation on every entity which
    fits some criteria (eg. has certain components) and some logic
    to update its own state
    """
    name: str
    reads: List[str]
    writes: List[str]
    extends: List[str]
    no_update: bool

    def __init__(self, name: str, tags: str, body: str, namespace: str):
        super().__init__({
            "tags": Section(tags, [Tag]),
            "body": Section(body, [])
        }, namespace + "::" + name)
        self.name = name
        self.reads = []
        self.writes = []
        self.extends = []

        self.no_update = False

        for tag in self.section("tags").children():
            if tag.name == "reads":
                self.reads += tag.args
            if tag.name == "writes":
                self.writes += tag.args
            if tag.name == "extends":
                self.extends = tag.args
            if tag.name == "no_update":
                self.no_update = True

    def compile(self) -> str:
        # language=C++
        loops = "update();" if len(self.reads + self.writes) == 0 else reduce(
            lambda x, y: x + f"""
            for (auto e : {y.namespace}::entities()) {{
                update({",".join(map(lambda a: f"e.get<{a}>()", self.reads + self.writes))});
            }}""", filter(
                lambda x: x.components.issuperset(set(self.reads + self.writes)),
                archetypes
            ), "")

        inheritances = reduce(lambda x, y: x + y + ",", self.extends, "")

        # language=C++
        return f"""
        class {self.name} : {inheritances} public Harmony::Internal::System {{
        {"#warning No matching archetypes for this system" if loops == "" else ""}
        
            inline void _update() override {{
                {loops}
            }}
            {"inline" if self.no_update else ""} void update({",".join(list(map(lambda x: f" {x}&", self.writes)) +
                                  list(map(lambda x: f"const {x}&", self.reads)))}) {"{{}}" if self.no_update else ";"}
        
            {self.section("body").compile()}
        }};
        """
