# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.element import Section
from preprocessor.element.structlike import structlike, Structlike
from functools import reduce
from typing import Set, List
from regex import compile
from re import Match


index = 0
archetypes = []


@structlike("archetype")
class Archetype(Structlike):
    """
    Defines a particular collection of components which form an entity
    which can be created and modified and destroyed
    """
    name: str
    index: int
    components: Set[str]
    constructors: List[str]

    def __init__(self, name: str, tags: str, body: str, namespace: str):
        global index
        super().__init__({
            "tags": Section(tags, []),
            "body": Section(body, [])
        }, namespace + "::" + name)
        self.name = name
        self.index = index
        self.constructors = []
        index += 1

        def extract(match: Match) -> str:
            self.constructors.append(match.group(0))
            return ""

        # language=regexp
        self.components = set(map(
            lambda x: x.strip(),
            compile(self.name + r"\s*\(.*?\);").sub(extract, body).split(",")
        ))
        archetypes.append(self)

    def compile(self) -> str:
        # language=C++
        specializations = reduce(lambda x, y: x + f"""
        template<> bool has<{y}>() {{ return true; }}
        template<> {y} get<{y}>() {{ return _get<{y}>(); }}
        """, self.components, "")
        # language=C++
        bc = reduce(lambda x, y: x + f"""
        get<{y}>().init();
        """, self.components, "")
        cs = "\n".join(self.constructors)

        # language=C++
        return f"""
        class {self.name} : public Harmony::Internal::SpecificEntity<{self.index}> {{
        public:
            inline {self.name}() : SpecificEntity() {{
                {bc}
            }}
            {cs}
        
            template<typename T> bool has() {{ return false; }}
            template<typename T> T get();
            {specializations}
        }};
        """
