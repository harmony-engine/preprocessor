# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from preprocessor.element import Section
from preprocessor.element.structlike import structlike, Structlike
from preprocessor.element.annotation.field import Field
from functools import reduce
from uuid import uuid4
from typing import List
from regex import compile
from re import Match


index = 0
field_index = 0


@structlike("component")
class Component(Structlike):
    """
    An object which looks and behaves like a struct, but is attached to
    an entity.
    """
    name: str
    index: int

    def __init__(self, name: str, tags: str, body: str, namespace: str):
        global index
        super().__init__({
            "tags": Section(tags, []),
            "body": Section(body, [Field])
        }, namespace + "::" + name)
        self.name = name
        self.index = index
        index += 1

    def compile(self) -> str:
        global field_index
        constructors = []
        fields = ""
        for child in self.section("body").children():
            if type(child) is not Field:
                continue
            constructor = "_" + str(uuid4()).replace("-", "_")
            constructors.append(constructor)
            putter = "_" + str(uuid4()).replace("-", "_")
            getter = "_" + str(uuid4()).replace("-", "_")
            # language=C++
            fields += f"""
            inline void {constructor}() {{
                new (load<{child.type}>({field_index})) {child.type}();
            }}
            inline void {putter}({child.type} v) {{
                *load<{child.type}>({field_index}) = v;
            }}
            inline {child.type} {getter}() const {{
                return *load<{child.type} const>({field_index});
            }}
            __declspec(property(put={putter}, get={getter})) {child.type} {child.name};
            """
            field_index += 1
        constructor = reduce(
            lambda x, y: x + f"""
            {y}();
            """, constructors, ""
        )
        # language=C++
        return f"""
        class {self.name} : private Harmony::Internal::Component {{
            template<Harmony::Internal::Index> friend class Harmony::Internal::SpecificEntity;
            friend Harmony::Entity;
        
        public:
            inline void init() {{
                {constructor}
            }}
        
            using Harmony::Internal::Component::Component;
            {fields}
            {self.section("body").compile()}
            static constexpr int type_id = {self.index};
        }};
        """
