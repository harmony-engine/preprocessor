# Copyright (C) 2020 Evan La Fontaine
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.


from os import popen
from os.path import join, basename
from typing import List
from pathlib import Path
import time

from worker import parallel, set_threads
from preprocessor.file import File
from preprocessor.element.structlike import Structlike


def process(out_dir: str, includes: List[str], files: List[str], jobs: int):
    processed = {}
    set_threads(jobs)

    @parallel
    def preprocess_files(file):
        nonlocal processed
        processed[file] = File(popen("clang++ -std=c++20 -E " + file + " -I".join(['', *includes])).read())

    preprocess_files(files)

    Structlike.compile_all()

    file_counter = 0
    file_ids = {}
    for file in files:
        file_ids[file] = file_counter
        file_counter += 1

    @parallel
    def compile_files(file):
        nonlocal file_ids
        Path(file).parent.mkdir(parents=True, exist_ok=True)
        with open(join(out_dir, f"{str(file_ids[file]).rjust(8, '0')}-{basename(file)}"), 'w') as out_file:
            out_file.write(processed[file].compile())

    compile_files(files)
